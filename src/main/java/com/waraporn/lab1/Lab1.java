/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.waraporn.lab1;

import java.util.Scanner;

/**
 *
 * @author DELL
 */
public class Lab1 {

    private char[][] board;
    public boolean play = false;
    public String turn;

    public void Start() {
        Scanner sc = new Scanner(System.in);
        String start;
        System.out.println(" ______________________________");
        System.out.println("|                              |");
        System.out.println("|      Welcome to OX Game      |");
        System.out.println("|______________________________|");

        System.out.print("Start Game Now? (y/n) : ");
        start = sc.nextLine().toLowerCase();
        while (!start.equals("n") && !start.equals("y")) {
            System.out.print("Please Try Again (y/n): ");
            start = sc.nextLine().toLowerCase();
        }
        if (start.equals("n")) {
            play = false;
        } else {
            play = true;
        }
    }

    private void OXboard() {
        System.out.println("_______________");
        for (int i = 0; i < 3; i++) {
            System.out.print(" | ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("_______________");
        }
    }

    public void newOXboard() {
        board = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '_';

            }
        }
    }

    public boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] != '_' && board[i][0] == board[i][1] && board[i][0] == board[i][2]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkColumns() {
        for (int i = 0; i < 3; i++) {
            if (board[0][i] != '_' && board[0][i] == board[1][i] && board[0][i] == board[2][i]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDiagonals() {
        if (board[0][0] != '_' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
            return true;
        }

        if (board[0][2] != '_' && board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '_') {
                    return false;
                }
            }
        }
        return true;
    }

    public void playGame() {
        Scanner sc = new Scanner(System.in);
        newOXboard();
        char Player = 'O';
        boolean End = false;
        OXboard();

        while (!End) {
            System.out.println("It's player " + Player + " turn.");
            System.out.print("Please input row[1-3] for your board : ");
            int row = sc.nextInt() - 1;
            System.out.print("Please input column[1-3] for your board : ");
            int col = sc.nextInt() - 1;

            if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '_') {
                board[row][col] = Player;
                OXboard();

                if (checkRow() || checkColumns() || checkDiagonals()) {
                    System.out.println(" ____________________________________ ");
                    System.out.println("|                                    |");
                    System.out.println("|  Congratulations! Player " + Player + " wins!!! |");
                    System.out.println("|____________________________________|");
                    End = true;

                } else if (checkDraw()) {
                    System.out.println(" ____________________");
                    System.out.println("|                    |");
                    System.out.println("|        Draw!       |");
                    System.out.println("|____________________|");
                    End = true;
                } else {
                    Player = (Player == 'O') ? 'X' : 'O';
                }
            } else {
                System.out.println("Please try agian!!.");
            }
        }
        System.out.print("Would you like to play again? (y/n) : ");
        String newGame = sc.next();
        if (newGame.equals("y")) {
            playGame();
        } else {
            System.out.println(" ____________________");
            System.out.println("|                    |");
            System.out.println("|     Exit Game      |");
            System.out.println("|      ByeBye!!      |");
            System.out.println("|____________________|");
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Lab1 oxGame = new Lab1();
        oxGame.Start();
        if (oxGame.play == false) {
            System.out.println(" ____________________");
            System.out.println("|                    |");
            System.out.println("|     Exit Game      |");
            System.out.println("|      ByeBye!!      |");
            System.out.println("|____________________|");
            System.exit(0);
        }
        oxGame.playGame();
    }
}
